#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

// gcc whois.c -o whois

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s [domain]\n", argv[0]);
        printf("Example: %s duckduckgo.com\n", argv[0]);
        return 1;
    }

    char *domain = argv[1];
    char command[256];
    snprintf(command, sizeof(command), "whois %s", domain);
    
    FILE *whois_output = popen(command, "r");

    if (whois_output == NULL) {
        perror("Error opening 'whois' command");
        return 1;
    }

    char buffer[128];

    while (fgets(buffer, sizeof(buffer), whois_output) != NULL) {
        printf("%s", buffer);
    }

    pclose(whois_output);

    return 0;
}
